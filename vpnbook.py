# coding: utf-8
from __future__ import print_function
from __future__ import unicode_literals
import argparse
import lxml.html

try:
    # Python 3
    from urllib.request import urlopen
except ImportError:
    # Python 2
    from urllib import urlopen


__author__ = 'iBiZoNiX'
__license__ = 'Creative Commons (CC BY 4.0)'
__version__ = '1.0.0'
__email__ = 'iBiZoNiX@outlook.com'
__date__ = '21.06.2016'


class VPNBookException(Exception):
    pass


class VPNBookIsNotAvailable(VPNBookException):
    pass


class VPNBookUnableToParseCredentials(VPNBookException):
    pass


class VPNBookConfigurationFileIsNotWritable(VPNBookException):
    pass


class VPNBook(object):
    """
    Provides an interface to communicate with http://vpnbook.com
    """
    def __init__(self, configuration_file=None):
        """
        Initialize default data.
        """
        self.url = 'http://www.vpnbook.com/'
        self.configuration_file = configuration_file

    def get_credentials(self):
        """
        Parses and returns credentials.
        :returns: credentials (username, password)
        :rtype: tuple
        """
        try:
            response = urlopen(self.url)
        except IOError:
            raise VPNBookIsNotAvailable('{0} is not available'.format(self.url))
        else:
            try:
                html = lxml.html.fromstring(response.read())
                credentials_div = html.get_element_by_id('openvpn').getchildren()[1]
                username = credentials_div.getchildren()[-2].text_content().split(' ')[1]
                password = credentials_div.getchildren()[-1].text_content().split(' ')[1]
            except Exception:
                raise VPNBookUnableToParseCredentials('unable to parse credentials')
            else:
                return username, password

    def save_credentials(self, credentials):
        """
        Saves credentials to specified configuration file.
        :param credentials: credentials (username, password)
        :type credentials: tuple
        """
        try:
            configuration_file = open(self.configuration_file, 'w')
            configuration_file.write('\n'.join(credentials))
        except PermissionError:
            raise VPNBookConfigurationFileIsNotWritable('Configuration file is not writable')
        else:
            configuration_file.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Parses credentials on http://vpnbook.com for free vpn and saves it to specified config file.'
    )
    parser.add_argument(
        '-c',
        '--configuration-file',
        type=str,
        required=True,
        help='path to configuration file where will be saved credentials',
        dest='configuration_file'
    )
    arguments = parser.parse_args()
    try:
        parser = VPNBook(configuration_file=arguments.configuration_file)
        parser.save_credentials(parser.get_credentials())
    except VPNBookException as error:
        print(error)
